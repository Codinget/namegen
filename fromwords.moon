minlen=4
maxlen=10

mingenlen=6
maxgenlen=12

numgen=20

maxvowels=2
maxconsonants=2
minvowelratio=1/3
maxvowelratio=1/2

minhamming=2

acceptableinput=(word) ->
		return not (#word>maxgenlen or #word<mingenlen)

allwords=do
		fd=io.open 'words'
		lines=[line for line in fd\lines!]
		fd\close!
		lines
words=[word for word in *allwords when acceptableinput word]
print "Loaded #{#words} words out of the #{#allwords} available"

reverse={word, true for word in *allwords}
print "Created reverse lookup"

bylen=do
		longest=0
		for word in *allwords
				longest=#word if #word>longest
		[ [word for word in *allwords when #word==len] for len=1, longest]
print "Created bylen index"

math.randomseed os.time!

exists=(word) -> reverse[word] or false

lettertype=(c) -> (c\match '[aiueo]') and 'v' or 'c'

acceptabledistribution=(word) ->
		ty, cnt=nil, 0
		nv, nc=0, 0
		for c in *[word\sub i, i for i=1, #word]
				t=lettertype c
				if t==ty
						cnt+=1
				else
						cnt=1
				ty=t
				if t=='v'
						nv+=1
						return false if cnt>maxvowels
				else
						nc+=1
						return false if cnt>maxconsonants
		return nv/nc>maxvowelratio or nv/nc<minvowelratio

hamming=(a, b) ->
		d=0
		for i=1, math.max #a, #b
				d+=1 if (a\sub i, i)!=(b\sub i, i)
		d

acceptablehamming=(word) ->
		for w in *(bylen[#word] or {})
				return false if minhamming>hamming w, word
		true

pick=(len) ->
		word=words[math.random #words]
		if #word<=len
				if #word<=3 or 1==math.random 5
						return word, word
		excess=#word-len+math.random maxgenlen-mingenlen
		local offset
		if excess>0
				offset=math.random excess
		else
				offset=0
		return (word\sub offset, len+offset-1), word

generate=() ->
		parts, len, originals={}, 0, {}
		while len<mingenlen
				picked, original=pick maxgenlen-len-math.random 0, maxgenlen-mingenlen
				continue if len+#picked>maxgenlen
				continue if len==0 and #picked>=mingenlen
				table.insert parts, picked
				table.insert originals, original
				len+=#picked
		generated=table.concat parts, ''
		if (exists generated) or not (acceptabledistribution generated) or not (acceptablehamming generated)
				return generate!
		return generated, originals

print ''
for i=1, numgen
		word, list=generate!
		print word, "(#{table.concat list, ','})"
