import p from require 'moon'

local letters, vowels, consonants

contains=(list, e) ->
		for i in *list
				return true if i==e
		return false

choice=(list) -> list[math.random 1, #list]
chartype=(c) ->
		return 'v' if contains vowels, c
		return 'c'

letters=[string.char i for i=(string.byte 'a'), (string.byte 'z')]
vowels={'a', 'i', 'u', 'e', 'o', 'y'}
consonants=[i for i in *letters when not contains vowels, i]

generate=(len) ->
		chars={}
		o1, o2=nil, nil
		for i=1, len
			local c
			if o1==o2 and o1!=nil
				c=choice vowels if o1=='c'
				c=choice consonants if o1=='v'
			else
				c=choice letters
			o1, o2, chars[i]=o2, (chartype c), c
		chars[1]=chars[1]\upper!
		table.concat chars, ''

math.randomseed os.time!

print 'Name length'
len=tonumber io.read '*l'
print generate len for i=1, 20

